# tools
Sprite_Studio CC0

sprite_studio.blend contain the follow CC0 resources:

----------------
Vinchau (https://www.blendswap.com/profile/321006)

Body Chan
https://www.blendswap.com/blend/23521

Body Mechanics Rigs
https://www.blendswap.com/blend/19110
----------------
VMComix
https://www.blendswap.com/blend/3115
----------------

They were so kindly to share their work in very permissive license which allow you do basically anything (except, of course, lying and claim owneship): it is not required to credit them (as you're not required to credit me) but, in the interest of common interest for such great resources they provide us, I do strongly advise you to.

